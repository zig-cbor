// Copyright © 2023 Gregory Oakes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the “Software”), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const std = @import("std");
const cbor = @import("cbor");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var stack_allocator = std.heap.stackFallback(4096, gpa.allocator());

    // Get a handle to buffered stdin.
    var stdin_file = std.io.getStdIn();
    var stdin_buffered = std.io.bufferedReader(stdin_file.reader());
    const stdin = stdin_buffered.reader();

    // Get a handle to buffered stdout.
    var stdout_file = std.io.getStdOut();
    var stdout_buffered = std.io.bufferedWriter(stdout_file.writer());
    const stdout = stdout_buffered.writer();

    // Get a handle to stderr.
    var stderr_file = std.io.getStdErr();
    const stderr = stderr_file.writer();

    while (true) {
        const alloc = stack_allocator.get();
        const item = cbor.next(stdin) catch |err| switch (err) {
            error.EndOfStream => break,
            else => return err,
        };
        item.diagnostic(alloc, stdout) catch |err| {
            try stderr.print("{any}\n", .{err});
            continue;
        };
        try stdout.writeByte('\n');
        try stdout_buffered.flush();
    }

    try stdout_buffered.flush();
}
