// Copyright © 2023 Gregory Oakes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the “Software”), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const std = @import("std");

/// The major tags defined in CBOR.
const Major = enum(u3) {
    Unsigned,
    Negative,
    ByteString,
    Text,
    Array,
    Map,
    Tag,
    Misc,
};

/// A CBOR header byte.
const Header = packed struct(u8) {
    arg: u5,
    major: Major,
};

const Token = union(Major) {
    const Self = @This();

    Unsigned: usize,
    Negative: usize,
    ByteString: ?usize,
    Text: ?usize,
    Array: ?usize,
    Map: ?usize,
    Tag: usize,
    Misc: Misc,

    const Misc = union(enum) {
        Simple: u8,
        Float: f64,
        Break,
    };

    pub fn intoDataItem(self: Self, reader: anytype) error{Malformed}!DataItem(@TypeOf(reader)) {
        return switch (self) {
            .Array => |len| .{ .Array = .{ .reader = reader, .remaining = len } },
            .Map => |len| .{ .Map = .{
                .reader = reader,
                .remaining = if (len) |l| l * 2 else null,
            } },
            inline .ByteString, .Text => |maybe_len, tag| @unionInit(
                DataItem(@TypeOf(reader)),
                @tagName(tag),
                if (maybe_len) |len| .{
                    .Definite = std.io.limitedReader(reader, len),
                } else .{
                    .Indefinite = .{ .reader = reader },
                },
            ),
            .Misc => |misc| switch (misc) {
                .Simple => |simple| switch (simple) {
                    0x14 => .{ .Bool = false },
                    0x15 => .{ .Bool = true },
                    0x16 => .Null,
                    else => .{ .Simple = simple },
                },
                .Float => |f| .{ .Float = f },
                .Break => error.Malformed,
            },
            .Unsigned => |uint| .{ .Unsigned = uint },
            .Negative => |nint| .{ .Negative = nint },
            .Tag => |tag| .{ .Tag = .{ .tag = tag, .reader = reader } },
        };
    }
};

/// The errors which may be produced while reading a token.
pub fn Error(comptime R: type) type {
    return R.Error || error{
        EndOfStream,
        Malformed,
        Overflow,
    };
}

/// The kinds of data items represented in the CBOR data model.
pub const DataItemTag = enum {
    Unsigned,
    Negative,
    ByteString,
    Text,
    Array,
    Map,
    Tag,
    Bool,
    Simple,
    Float,
    Null,
};

/// The compile time options for decoding `T`.
pub fn DecodeOptions(comptime T: type) type {
    switch (@typeInfo(T)) {
        .Pointer => |info| switch (info.size) {
            .One => return DecodeOptions(info.child),
            .Slice => if (info.child == u8) {
                return struct {
                    byte_string: bool = true,
                    text: bool = true,
                    array: bool = true,
                    child: DecodeOptions(info.child) = .{},
                    max_size: usize = std.math.maxInt(usize),
                };
            } else {
                return struct {
                    array: bool = true,
                    child: DecodeOptions(info.child) = .{},
                    max_size: usize = std.math.maxInt(usize),
                };
            },
            else => {},
        },
        inline .Array, .Vector => |info| return DecodeOptions([]const info.child),
        .Struct => |info| return struct {
            array: bool = true,
            map_integer: bool = true,
            duplicates: enum { Error, First, Last } = .Error,
            fields: FieldOpts(info.fields, DecodeOptions) = .{},
            labels: ?type = null,
        },
        .Optional => |info| return DecodeOptions(info.child),
        .Enum => return EnumDecoding,
        .Union => |info| return struct {
            encoding: union(enum) {
                External: EnumDecoding,
                Array: EnumDecoding,
                Tagged,
            } = .{ .Array = .{} },
            fields: FieldOpts(info.fields, DecodeOptions) = .{},
        },
        else => {},
    }
    return struct {};
}

const EnumDecoding = struct {
    text: bool = true,
    integer: bool = true,
};

/// The errors which may be produced while decoding `T` from a `DataItem`.
pub fn DecodeError(comptime R: type, comptime T: type) type {
    return switch (@typeInfo(T)) {
        .Void, .Bool => error{UnexpectedToken},
        .Int => error{ UnexpectedToken, Overflow },
        .Float => error{UnexpectedToken},
        .Pointer => |info| switch (info.size) {
            .Slice => error{StreamTooLong},
            else => error{},
        } ||
            std.mem.Allocator.Error ||
            DecodeError(R, info.child) ||
            Error(R),
        .Array => |info| error{StreamTooLong} || DecodeError(R, info.child) || Error(R),
        .Struct => |info| error{
            UnexpectedToken,
            MissingField,
            DuplicateField,
            UnknownTag,
        } || blk: {
            var set = error{};
            inline for (info.fields) |fld|
                set = set || DecodeError(R, fld.type);
            break :blk set;
        } || Error(R),
        .Optional => |info| DecodeError(R, info.child),
        .Enum => |info| error{UnknownTag} || DecodeError(R, info.tag_type) || Error(R),
        .Union => |info| error{
            UnexpectedToken,
            ExpectedTag,
            UnknownTag,
            TooManyItems,
        } || blk: {
            var set = error{};
            inline for (info.fields) |fld|
                set = set || DecodeError(R, fld.type);
            break :blk set;
        } || Error(R),

        else => error{},
    };
}

/// A single data item within the CBOR data model.
///
/// All possible values representable in CBOR are also representable within a `DataItem`. The only
/// exception is when `usize` is less than `u64`.
pub fn DataItem(comptime R: type) type {
    return union(DataItemTag) {
        const Self = @This();

        Unsigned: usize,
        Negative: usize,
        ByteString: ByteStringReader(R),
        Text: TextReader(R),
        Array: Sequence(R),
        Map: Sequence(R),
        Tag: Tagged(R),
        Bool: bool,
        Simple: u8,
        Float: f64,
        Null,

        /// Decode `self` into `T`.
        pub fn decode(
            self: *Self,
            comptime T: type,
            alloc: ?std.mem.Allocator,
            comptime opts: DecodeOptions(T),
        ) DecodeError(R, T)!T {
            switch (@typeInfo(T)) {
                inline else => |info, tag| {
                    const func_name = "decodeAs" ++ @tagName(tag);
                    if (@hasDecl(Self, func_name)) {
                        return @field(Self, func_name)(self, alloc, .{
                            .type = T,
                            .opts = opts,
                            .info = info,
                        });
                    }
                    @compileError("unable to generically decode " ++ @typeName(T));
                },
            }
        }

        inline fn decodeAsVoid(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            _ = alloc;
            try self.unwrap(.Null);
        }

        inline fn decodeAsBool(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            _ = alloc;
            return try self.unwrap(.Bool);
        }

        inline fn decodeAsInt(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            _ = alloc;
            switch (self.*) {
                .Unsigned => |uint| return std.math.cast(ct.type, uint) orelse error.Overflow,
                .Negative => |nint| if (ct.info.signedness == .signed) {
                    if (std.math.cast(ct.type, nint)) |t| {
                        return -t - 1;
                    }
                    return error.Overflow;
                },
                else => {},
            }
            return error.UnexpectedToken;
        }

        inline fn decodeAsFloat(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            _ = alloc;
            return @floatCast(try self.unwrap(.Float));
        }

        inline fn decodeAsPointer(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            switch (ct.info.size) {
                inline else => |tag| {
                    const func_name = "decodeAsPointer" ++ @tagName(tag);
                    if (@hasDecl(Self, func_name)) {
                        return @field(Self, func_name)(self, alloc, ct);
                    }
                    @compileError("unable to decode pointer of this size " ++ @typeName(ct.type));
                },
            }
        }

        inline fn decodeAsPointerOne(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            const value: *align(ct.info.alignment) ct.info.child = @ptrCast(
                try alloc.?.alignedAlloc(u8, ct.info.alignment, @sizeOf(ct.info.child)),
            );
            errdefer alloc.?.destroy(value);
            value.* = try self.decode(ct.info.child, alloc, ct.opts);
            return value;
        }

        inline fn decodeAsPointerSlice(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            switch (self.*) {
                inline .Array, .ByteString, .Text => |*payload, tag| {
                    const func_name = "decodeAsPointerSliceFrom" ++ @tagName(tag);
                    return @field(Self, func_name)(payload, alloc, ct);
                },
                else => return error.UnexpectedToken,
            }
        }

        inline fn decodeAsPointerSliceFromArray(
            seq: *Sequence(R),
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            if (!ct.opts.array)
                return error.UnexpectedToken;
            if (seq.sizeHint()) |len| {
                if (len > ct.opts.max_size)
                    return error.StreamTooLong;
                const slice = try alloc.?.allocWithOptions(
                    ct.info.child,
                    len,
                    ct.info.alignment,
                    std.meta.sentinel(ct.type),
                );
                errdefer alloc.?.free(slice);
                var idx: usize = 0;
                errdefer for (slice[0..idx]) |initialized|
                    genericFree(alloc.?, initialized);
                while (try seq.decode(ct.info.child, alloc, ct.opts.child)) |child| {
                    slice[idx] = child;
                    idx += 1;
                }
                return slice;
            }
            var arr = std.ArrayListAligned(
                ct.info.child,
                ct.info.alignment,
            ).init(alloc.?);
            defer arr.deinit();
            errdefer for (arr.items) |initialized|
                genericFree(alloc.?, initialized);
            var len: usize = 0;
            while (try seq.decode(ct.info.child, alloc, ct.opts.child)) |child| {
                errdefer genericFree(alloc.?, child);
                len += 1;
                if (len > ct.opts.max_size)
                    return error.StreamTooLong;
                try arr.append(child);
            }
            if (comptime std.meta.sentinel(ct.type)) |sentinel|
                return try arr.toOwnedSliceSentinel(sentinel);
            return try arr.toOwnedSlice();
        }

        inline fn decodeAsPointerSliceFromByteString(
            stream: *ByteStringReader(R),
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            if (!@hasField(@TypeOf(ct.opts), "byte_string") or !ct.opts.byte_string)
                return error.UnexpectedToken;
            const reader = stream.reader();
            if (stream.sizeHint()) |len| {
                if (len > ct.opts.max_size)
                    return error.StreamTooLong;
                const slice = try alloc.?.allocWithOptions(
                    ct.info.child,
                    len,
                    ct.info.alignment,
                    std.meta.sentinel(ct.type),
                );
                errdefer alloc.?.free(slice);
                try reader.readNoEof(slice);
                return slice;
            }
            var arr = std.ArrayListAligned(
                ct.info.child,
                ct.info.alignment,
            ).init(alloc.?);
            defer arr.deinit();
            // TODO: https://github.com/ziglang/zig/issues/18539
            reader.any().readAllArrayListAligned(
                ct.info.alignment,
                &arr,
                ct.opts.max_size,
            ) catch |err| {
                return @as(DecodeError(R, ct.type), @errorCast(err));
            };
            if (comptime std.meta.sentinel(ct.type)) |sentinel|
                return try arr.toOwnedSliceSentinel(sentinel);
            return try arr.toOwnedSlice();
        }

        inline fn decodeAsPointerSliceFromText(
            stream: *TextReader(R),
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            if (!@hasField(@TypeOf(ct.opts), "text") or !ct.opts.text)
                return error.UnexpectedToken;
            const reader = stream.reader();
            if (stream.sizeHint()) |len| {
                if (len > ct.opts.max_size)
                    return error.StreamTooLong;
                const slice = try alloc.?.allocWithOptions(
                    ct.info.child,
                    len,
                    ct.info.alignment,
                    std.meta.sentinel(ct.type),
                );
                errdefer alloc.?.free(slice);
                try reader.readNoEof(slice);
                std.debug.assert(std.unicode.utf8ValidateSlice(slice));
                return slice;
            }
            var arr = std.ArrayListAligned(
                ct.info.child,
                ct.info.alignment,
            ).init(alloc.?);
            defer arr.deinit();
            // TODO: https://github.com/ziglang/zig/issues/18539
            reader.any().readAllArrayListAligned(
                ct.info.alignment,
                &arr,
                ct.opts.max_size,
            ) catch |err| {
                return @as(DecodeError(R, ct.type), @errorCast(err));
            };
            if (comptime std.meta.sentinel(ct.type)) |sentinel|
                return try arr.toOwnedSliceSentinel(sentinel);
            std.debug.assert(std.unicode.utf8ValidateSlice(arr.items));
            return try arr.toOwnedSlice();
        }

        inline fn decodeAsArray(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            var result: ct.type = undefined;
            var idx: usize = 0;
            errdefer for (result[0..idx]) |initialized|
                genericFree(alloc.?, initialized);
            switch (self.*) {
                .Array => |*seq| {
                    if (comptime !ct.opts.array)
                        return error.UnexpectedToken;
                    while (try seq.decode(ct.info.child, alloc, ct.opts.child)) |child| {
                        if (idx > ct.info.len)
                            return error.StreamTooLong;
                        result[idx] = child;
                        idx += 1;
                    }
                },
                .ByteString => |*stream| {
                    if (comptime !@hasField(@TypeOf(ct.opts), "byte_string") or !ct.opts.byte_string)
                        return error.UnexpectedToken;
                    idx += try stream.reader().readAll(&result);
                },
                .Text => |*stream| {
                    if (comptime !@hasField(@TypeOf(ct.opts), "text") or !ct.opts.text)
                        return error.UnexpectedToken;
                    idx += try stream.reader().readAll(&result);
                    std.debug.assert(std.unicode.utf8ValidateSlice(result[0..idx]));
                },
                else => return error.UnexpectedToken,
            }
            if (comptime std.meta.sentinel(ct.type)) |sentinel| {
                result[idx] = sentinel;
            } else if (idx < ct.info.len) {
                return error.EndOfStream;
            }
            return result;
        }

        inline fn decodeAsStruct(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            const FieldEnum = ct.opts.labels orelse std.meta.FieldEnum(ct.type);
            comptime {
                for (std.meta.fieldNames(ct.type)) |name| {
                    std.debug.assert(@hasField(FieldEnum, name));
                }
                for (std.meta.fieldNames(FieldEnum)) |name| {
                    std.debug.assert(@hasField(ct.type, name));
                }
            }
            var result: ct.type = undefined;
            var unseen = std.EnumSet(FieldEnum).initFull();
            errdefer inline for (ct.info.fields) |fld| {
                if (!unseen.contains(@field(FieldEnum, fld.name))) {
                    genericFree(alloc, @field(result, fld.name));
                }
            };
            switch (self.*) {
                .Array => |*seq| {
                    if (!ct.opts.array)
                        return error.UnexpectedToken;
                    inline for (ct.info.fields) |fld| {
                        if (try seq.decode(
                            fld.type,
                            alloc,
                            @field(ct.opts.fields, fld.name),
                        )) |value| {
                            @field(result, fld.name) = value;
                            unseen.remove(@field(FieldEnum, fld.name));
                        }
                    }
                },
                .Map => |*seq| {
                    if (!ct.opts.map_integer)
                        return error.UnexpectedToken;
                    while (try seq.decode(FieldEnum, alloc, .{})) |label| {
                        if (ct.info.fields.len == 0)
                            unreachable;
                        if (!unseen.contains(label)) {
                            switch (ct.opts.duplicates) {
                                .Error => return error.DuplicateField,
                                .First => {
                                    var value = try seq.next() orelse
                                        return error.Malformed;
                                    try value.skip();
                                    continue;
                                },
                                .Last => switch (label) {
                                    inline else => |ct_label| {
                                        genericFree(alloc, @field(result, @tagName(ct_label)));
                                        unseen.insert(label);
                                    },
                                },
                            }
                        }
                        switch (label) {
                            inline else => |ct_label| {
                                @field(result, @tagName(ct_label)) = try seq.decode(
                                    std.meta.FieldType(ct.type, @field(
                                        std.meta.FieldEnum(ct.type),
                                        @tagName(ct_label),
                                    )),
                                    alloc,
                                    @field(ct.opts.fields, @tagName(ct_label)),
                                ) orelse return error.Malformed;
                            },
                        }
                        unseen.remove(label);
                    }
                },
                else => return error.UnexpectedToken,
            }
            inline for (ct.info.fields) |fld| {
                if (fld.default_value) |def| {
                    const label = @field(FieldEnum, fld.name);
                    if (unseen.contains(label)) {
                        @field(result, fld.name) = @as(*const fld.type, @alignCast(@ptrCast(def))).*;
                        unseen.remove(label);
                    }
                }
            }
            if (unseen.count() > 0)
                return error.MissingField;
            return result;
        }

        inline fn decodeAsOptional(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            if (self.* == .Null)
                return null;
            return try self.decode(ct.info.child, alloc, ct.opts);
        }

        inline fn decodeAsEnum(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            switch (self.*) {
                .Text => |*text| {
                    if (!ct.opts.text)
                        return error.UnexpectedToken;
                    const field_names = comptime std.meta.fieldNames(ct.type);
                    const max_len = comptime blk: {
                        var max = 0;
                        for (field_names) |name|
                            max = @max(max, name.len);
                        break :blk max;
                    };
                    var buf: [max_len + 1]u8 = undefined;
                    const reader = text.reader();
                    const nread = try reader.readAll(&buf);
                    if (nread > max_len) {
                        try text.exhaust();
                        return error.UnknownTag;
                    }
                    return std.meta.stringToEnum(ct.type, buf[0..nread]) orelse
                        error.UnknownTag;
                },
                else => {
                    if (!ct.opts.integer)
                        return error.UnexpectedToken;
                    const Int = std.meta.Tag(ct.type);
                    const int = self.decode(Int, alloc, .{}) catch |err| switch (err) {
                        error.Overflow => return error.UnknownTag,
                        else => |e| return e,
                    };
                    return std.meta.intToEnum(ct.type, int) catch
                        return error.UnknownTag;
                },
            }
        }

        fn decodeAsUnion(
            self: *Self,
            alloc: ?std.mem.Allocator,
            comptime ct: anytype,
        ) DecodeError(R, ct.type)!ct.type {
            switch (ct.opts.encoding) {
                inline .External, .Array => |container_decoding, encoding| {
                    const container_type = if (encoding == .External)
                        .Map
                    else
                        .Array;
                    var container = try self.unwrap(container_type);
                    const Tag = std.meta.Tag(ct.type);
                    const tag = try container.decode(Tag, alloc, container_decoding) orelse
                        return error.ExpectedTag;
                    var content_item = try container.next() orelse
                        return error.Malformed;
                    const result = switch (tag) {
                        inline else => |ct_tag| @unionInit(
                            ct.type,
                            @tagName(ct_tag),
                            try content_item.decode(
                                std.meta.FieldType(ct.type, ct_tag),
                                alloc,
                                @field(ct.opts.fields, @tagName(ct_tag)),
                            ),
                        ),
                    };
                    errdefer genericFree(alloc, result);
                    if (try container.exhaust())
                        return error.TooManyItems;
                    return result;
                },
                .Tagged => {
                    var tagged = try self.unwrap(.Tag);
                    const tag = std.meta.intToEnum(std.meta.Tag(ct.type), tagged.tag) catch
                        return error.UnknownTag;
                    var content_item = try tagged.content();
                    switch (tag) {
                        inline else => |ct_tag| return @unionInit(
                            ct.type,
                            @tagName(ct_tag),
                            try content_item.decode(
                                std.meta.FieldType(ct.type, ct_tag),
                                alloc,
                                @field(ct.opts.fields, @tagName(ct_tag)),
                            ),
                        ),
                    }
                },
            }
        }

        fn unwrap(
            self: Self,
            comptime kind: DataItemTag,
        ) error{UnexpectedToken}!std.meta.TagPayload(Self, kind) {
            return switch (self) {
                kind => |payload| payload,
                else => error.UnexpectedToken,
            };
        }

        /// Skip this data item, leaving the stream just prior to the next data item.
        pub fn skip(self: *Self) Error(R)!void {
            switch (self.*) {
                inline .ByteString, .Text => |*str_reader| {
                    try str_reader.exhaust();
                },
                .Array => |*iter| {
                    _ = try iter.exhaust();
                },
                .Map => |*iter| {
                    var idx: usize = 0;
                    while (true) {
                        var child = try iter.next() orelse
                            break;
                        try child.skip();
                        idx +%= 1;
                    }
                    if (idx % 2 == 1) {
                        return error.Malformed;
                    }
                },
                .Tag => |tagged| {
                    var content = try tagged.content();
                    try content.skip();
                },
                else => {},
            }
        }

        /// Write diagnostics of this data item to `writer`.
        pub fn diagnostic(self: Self, alloc: std.mem.Allocator, writer: anytype) !void {
            const Layer = struct {
                item: Self,
                first: bool = true,
                toggle: u1 = 0,
            };

            var stack = std.SegmentedList(Layer, 1){};
            defer stack.deinit(alloc);

            try stack.append(alloc, .{ .item = self });

            // Every branch which doesn't continue early pops a layer off the stack.
            while (stack.len > 0) {
                const head = stack.at(stack.len - 1);

                switch (head.item) {
                    .Unsigned => |uint| try writer.print("{d}", .{uint}),
                    .Negative => |nint| {
                        // Gracefully handle max int without casting to i128.
                        if (std.math.add(usize, nint, 1)) |i| {
                            try writer.print("-{d}", .{i});
                        } else |_| {
                            const one_too_many = std.math.maxInt(usize) + 1;
                            try writer.writeAll(std.fmt.comptimePrint("-{d}", .{one_too_many}));
                        }
                    },
                    .ByteString => |*bytes| {
                        var fifo = std.fifo.LinearFifo(u8, .{ .Static = 512 }).init();
                        var hex_stream = hexStream(writer);
                        const hex_writer = hex_stream.writer();
                        switch (bytes.*) {
                            .Definite => |*chunk| {
                                try writer.writeAll("h\"");
                                try fifo.pump(chunk.reader(), hex_writer);
                                try writer.writeByte('"');
                            },
                            .Indefinite => |*indef| {
                                try writer.writeAll("(_ ");
                                var first = true;
                                while (try indef.next()) |chunk_const| {
                                    if (first) {
                                        first = false;
                                    } else {
                                        try writer.writeAll(", ");
                                    }
                                    try writer.writeAll("h\"");
                                    var chunk = chunk_const;
                                    try fifo.pump(chunk.reader(), hex_writer);
                                    try writer.writeByte('"');
                                }
                                try writer.writeByte(')');
                            },
                        }
                    },
                    .Text => |*text| switch (text.*) {
                        .Definite => |*chunk| {
                            try writer.writeByte('"');
                            try semiJsonUtf8Escape(chunk.reader(), writer);
                            try writer.writeByte('"');
                        },
                        .Indefinite => |*indef| {
                            try writer.writeAll("(_ ");
                            var first = true;
                            while (try indef.next()) |chunk_const| {
                                if (first) {
                                    first = false;
                                } else {
                                    try writer.writeAll(", ");
                                }
                                try writer.writeByte('"');
                                var chunk = chunk_const;
                                try semiJsonUtf8Escape(chunk.reader(), writer);
                                try writer.writeByte('"');
                            }
                            try writer.writeAll(")");
                        },
                    },
                    .Array => |*seq| {
                        if (head.first) {
                            try writer.writeAll(if (seq.remaining == null) "[_ " else "[");
                        }
                        if (try seq.next()) |child| {
                            if (head.first) {
                                head.first = false;
                            } else {
                                try writer.writeAll(", ");
                            }
                            try stack.append(alloc, .{ .item = child });
                            continue;
                        }
                        try writer.writeByte(']');
                    },
                    .Map => |*map| {
                        if (head.first) {
                            try writer.writeAll(if (map.remaining == null) "{_ " else "{");
                        }
                        if (try map.next()) |child| {
                            if (head.first) {
                                head.first = false;
                            } else {
                                try writer.writeAll(switch (head.toggle) {
                                    0 => ": ",
                                    1 => ", ",
                                });
                                head.toggle +%= 1;
                            }
                            try stack.append(alloc, .{ .item = child });
                            continue;
                        }
                        try writer.writeByte('}');
                    },
                    .Tag => |*tagged| {
                        if (head.first) {
                            try writer.print("{d}(", .{tagged.tag});
                            head.first = false;
                            try stack.append(alloc, .{ .item = try tagged.content() });
                            continue;
                        }
                        try writer.writeByte(')');
                    },
                    .Bool => |b| try writer.writeAll(if (b) "true" else "false"),
                    .Simple => |simple| try writer.print("simple({d})", .{simple}),
                    .Float => |float| try std.json.stringify(float, .{}, writer),
                    .Null => try writer.writeAll("null"),
                }

                stack.len -= 1;
            }
        }
    };
}

fn expectDecode(
    bytes: []const u8,
    comptime T: type,
    expected: anyerror!T,
    comptime opts: DecodeOptions(T),
) !void {
    var counting = std.testing.FailingAllocator.init(std.testing.allocator, .{});
    try expectDecodeAlloc(bytes, T, expected, opts, counting.allocator());
    const max_alloc = counting.alloc_index;
    for (0..max_alloc) |fail_index| {
        var failing_alloc = std.testing.FailingAllocator.init(std.testing.allocator, .{
            .fail_index = fail_index,
        });
        try expectDecodeAlloc(bytes, T, error.OutOfMemory, opts, failing_alloc.allocator());
    }
}

fn expectDecodeAlloc(
    bytes: []const u8,
    comptime T: type,
    expected: anyerror!T,
    comptime opts: DecodeOptions(T),
    alloc: std.mem.Allocator,
) !void {
    var stream = std.io.fixedBufferStream(bytes);
    const result = decode(stream.reader(), T, alloc, opts);
    if (expected) |exp_val| {
        const value = try result;
        defer genericFree(alloc, value);
        try std.testing.expectEqualDeep(exp_val, try result);
    } else |exp_err| {
        defer if (result) |value| genericFree(alloc, value) else |_| {};
        try std.testing.expectError(exp_err, result);
    }
}

test "decode void" {
    try expectDecode("\xf6", void, {}, .{});
    try expectDecode("\x01", void, error.UnexpectedToken, .{});
}

test "decode bool" {
    try expectDecode("\xf4", bool, false, .{});
    try expectDecode("\xf5", bool, true, .{});
    try expectDecode("\x00", bool, error.UnexpectedToken, .{});
}

test "decode int" {
    try expectDecode("\x00", u64, 0, .{});
    try expectDecode("\x01", u64, 1, .{});
    try expectDecode("\x0a", u64, 10, .{});
    try expectDecode("\x17", u64, 23, .{});
    try expectDecode("\x18\x18", u64, 24, .{});
    try expectDecode("\x18\x19", u64, 25, .{});
    try expectDecode("\x18\x64", u64, 100, .{});
    try expectDecode("\x19\x03\xe8", u64, 1000, .{});
    try expectDecode("\x1a\x00\x0f\x42\x40", u64, 1000000, .{});
    try expectDecode("\x00", i128, 0, .{});
    try expectDecode("\x01", i128, 1, .{});
    try expectDecode("\x0a", i128, 10, .{});
    try expectDecode("\x17", i128, 23, .{});
    try expectDecode("\x18\x18", i128, 24, .{});
    try expectDecode("\x18\x19", i128, 25, .{});
    try expectDecode("\x18\x64", i128, 100, .{});
    try expectDecode("\x19\x03\xe8", i128, 1000, .{});
    try expectDecode("\x1a\x00\x0f\x42\x40", i128, 1000000, .{});
    if (@bitSizeOf(usize) >= 64) {
        try expectDecode("\x1b\x00\x00\x00\xe8\xd4\xa5\x10\x00", i128, 1000000000000, .{});
        try expectDecode("\x1b\xff\xff\xff\xff\xff\xff\xff\xff", i128, 18446744073709551615, .{});
        try expectDecode("\x3b\xff\xff\xff\xff\xff\xff\xff\xff", i128, -18446744073709551616, .{});
    } else {
        try expectDecode("\x1b\x00\x00\x00\xe8\xd4\xa5\x10\x00", i128, error.Overflow, .{});
        try expectDecode("\x1b\xff\xff\xff\xff\xff\xff\xff\xff", i128, error.Overflow, .{});
        try expectDecode("\x3b\xff\xff\xff\xff\xff\xff\xff\xff", i128, error.Overflow, .{});
    }
    try expectDecode("\x20", i128, -1, .{});
    try expectDecode("\x29", i128, -10, .{});
    try expectDecode("\x38\x63", i128, -100, .{});
    try expectDecode("\x39\x03\xe7", i128, -1000, .{});
    try expectDecode("\x19\x01\x00", u8, error.Overflow, .{});
    try expectDecode("\x39\x01\x00", u8, error.UnexpectedToken, .{});
    try expectDecode("\x39\x01\x00", i8, error.Overflow, .{});
    try expectDecode("\x19\x03\xe8", u8, error.Overflow, .{});
}

test "decode float" {
    try expectDecode("\xf9\x00\x00", f64, 0.0, .{});
    try expectDecode("\xf9\x80\x00", f64, -0.0, .{});
    try expectDecode("\xf9\x3c\x00", f64, 1.0, .{});
    try expectDecode("\xfb\x3f\xf1\x99\x99\x99\x99\x99\x9a", f64, 1.1, .{});
    try expectDecode("\xf9\x3e\x00", f64, 1.5, .{});
    try expectDecode("\xf9\x7b\xff", f64, 65504.0, .{});
    try expectDecode("\xfa\x47\xc3\x50\x00", f64, 100000.0, .{});
    try expectDecode("\xfa\x7f\x7f\xff\xff", f64, 3.4028234663852886E+38, .{});
    try expectDecode("\xfb\x7e\x37\xe4\x3c\x88\x00\x75\x9c", f64, 1.0E+300, .{});
    try expectDecode("\xf9\x00\x01", f64, 5.960464477539063E-8, .{});
    try expectDecode("\xf9\x04\x00", f64, 0.00006103515625, .{});
    try expectDecode("\xf9\xc4\x00", f64, -4.0, .{});
    try expectDecode("\xfb\xc0\x10\x66\x66\x66\x66\x66\x66", f64, -4.1, .{});
    try expectDecode("\xf9\x7c\x00", f64, std.math.inf(f64), .{});
    try expectDecode("\xf9\xfc\x00", f64, -std.math.inf(f64), .{});
    try expectDecode("\xfa\x7f\x80\x00\x00", f64, std.math.inf(f64), .{});
    try expectDecode("\xfa\xff\x80\x00\x00", f64, -std.math.inf(f64), .{});
    try expectDecode("\xfb\x7f\xf0\x00\x00\x00\x00\x00\x00", f64, std.math.inf(f64), .{});
    try expectDecode("\xfb\xff\xf0\x00\x00\x00\x00\x00\x00", f64, -std.math.inf(f64), .{});
    try expectDecode("\x00", f64, error.UnexpectedToken, .{});
}

test "decode single pointer" {
    try expectDecode("\x20", *const isize, &-1, .{});
    try expectDecode("mHello, world!", *const isize, error.UnexpectedToken, .{});
}

test "decode slice from byte string" {
    try expectDecode("\x4dHello, world!", []const u8, "Hello, world!", .{});
    try expectDecode("\x4dHello, world!", [:0]const u8, "Hello, world!", .{});
}

test "decode slice from byte string of indefinite length" {
    try expectDecode("\x5f\x47Hello, \x46world!\xff", []const u8, "Hello, world!", .{});
    try expectDecode("\x5f\x47Hello, \x46world!\xff", [:0]const u8, "Hello, world!", .{});
}

test "decode slice from text" {
    try expectDecode("\x6dHello, world!", []const u8, "Hello, world!", .{});
    try expectDecode("\x6dHello, world!", [:0]const u8, "Hello, world!", .{});
}

test "decode slice from text of indefinite length" {
    try expectDecode("\x7f\x67Hello, \x66world!\xff", []const u8, "Hello, world!", .{});
    try expectDecode("\x7f\x67Hello, \x66world!\xff", [:0]const u8, "Hello, world!", .{});
}

test "decode slice from array" {
    try expectDecode(
        "\x8d\x18H\x18e\x18l\x18l\x18o\x18,\x18 \x18w\x18o\x18r\x18l\x18d\x18!",
        []const u8,
        "Hello, world!",
        .{},
    );
    try expectDecode(
        "\x8d\x18H\x18e\x18l\x18l\x18o\x18,\x18 \x18w\x18o\x18r\x18l\x18d\x18!",
        [:0]const u8,
        "Hello, world!",
        .{},
    );
}

test "decode slice from array of indefinite length" {
    try expectDecode(
        "\x9f\x18H\x18e\x18l\x18l\x18o\x18,\x18 \x18w\x18o\x18r\x18l\x18d\x18!\xff",
        []const u8,
        "Hello, world!",
        .{},
    );
    try expectDecode(
        "\x9f\x18H\x18e\x18l\x18l\x18o\x18,\x18 \x18w\x18o\x18r\x18l\x18d\x18!\xff",
        [:0]const u8,
        "Hello, world!",
        .{},
    );
}

test "decode non-byte slice from array" {
    try expectDecode(
        "\x82\x1a\xde\xad\xbe\xef\x1a\xbe\xef\xde\xad",
        []const usize,
        &[_]usize{ 0xdeadbeef, 0xbeefdead },
        .{},
    );
}

test "decode slice from disallowed representation" {
    try expectDecode("\x40", []const u8, error.UnexpectedToken, .{ .byte_string = false });
    try expectDecode("\x60", []const u8, error.UnexpectedToken, .{ .text = false });
    try expectDecode("\x80", []const u8, error.UnexpectedToken, .{ .array = false });
}

test "decode slice w/ errors" {
    try expectDecode("\x42\x00", []const u8, error.EndOfStream, .{});
    try expectDecode("\x62\x00", []const u8, error.EndOfStream, .{});
    try expectDecode("\x82\x20\x01", []const u8, error.UnexpectedToken, .{});
    try expectDecode("\x82\x01\x20", []const *const u8, error.UnexpectedToken, .{});
    try expectDecode("MHello, world!", []const usize, error.UnexpectedToken, .{});
    try expectDecode("mHello, world!", []const usize, error.UnexpectedToken, .{});
    try expectDecode("\x20", []const u8, error.UnexpectedToken, .{});
    try expectDecode("\x42\x00\x00", []const u8, error.StreamTooLong, .{ .max_size = 1 });
    try expectDecode("\x62\x00\x00", []const u8, error.StreamTooLong, .{ .max_size = 1 });
    try expectDecode("\x82\x00\x00", []const u8, error.StreamTooLong, .{ .max_size = 1 });
    try expectDecode("\x5f\x41\x00\x41\x00\xff", []const u8, error.StreamTooLong, .{ .max_size = 1 });
    try expectDecode("\x7f\x61\x00\x61\x00\xff", []const u8, error.StreamTooLong, .{ .max_size = 1 });
    try expectDecode("\x9f\x00\x00\xff", []const u8, error.StreamTooLong, .{ .max_size = 1 });
}

test "decode array" {
    try expectDecode("\x43abc", [3]u8, "abc".*, .{});
    try expectDecode("\x63abc", [3]u8, "abc".*, .{});
    try expectDecode("\x83\x18a\x18b\x18c", [3]u8, "abc".*, .{});
    try expectDecode("\x43abc", [3:0]u8, "abc".*, .{});
    try expectDecode("\x63abc", [3:0]u8, "abc".*, .{});
    try expectDecode("\x83\x18a\x18b\x18c", [3:0]u8, "abc".*, .{});
    try expectDecode("\x43\x00\x00\x00", [3]u8, error.UnexpectedToken, .{ .byte_string = false });
    try expectDecode("\x63\x00\x00\x00", [3]u8, error.UnexpectedToken, .{ .text = false });
    try expectDecode("\x83\x00\x00\x00", [3]u8, error.UnexpectedToken, .{ .array = false });
    try expectDecode("\x82\x20\x01", [3]*const u8, error.UnexpectedToken, .{});
    try expectDecode("\x82\x01\x20", [3]*const u8, error.UnexpectedToken, .{});
    try expectDecode("\x20", [3]u8, error.UnexpectedToken, .{});
}

test "decode struct from array" {
    try expectDecode("\x80", struct {}, .{}, .{});
    try expectDecode("\x80", struct {}, error.UnexpectedToken, .{ .array = false });
    try expectDecode(
        "\x82\x19\xde\xad\x19\xbe\xef",
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        .{},
    );
}

test "decode struct from array w/ using default" {
    try expectDecode(
        "\x81\x19\xde\xad",
        struct { abc: usize, def: usize = 0xbeef },
        .{ .abc = 0xdead, .def = 0xbeef },
        .{},
    );
}

test "decode struct from map" {
    try expectDecode("\xa0", struct {}, .{}, .{});
    try expectDecode("\xa0", struct {}, error.UnexpectedToken, .{ .map_integer = false });
    try expectDecode(
        "\xa2\x00\x19\xde\xad\x01\x19\xbe\xef",
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        .{},
    );
}

test "decode struct from array w/ custom label integers" {
    try expectDecode(
        "\xa2\x02\x19\xde\xad\x20\x19\xbe\xef",
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        .{ .labels = enum(isize) {
            abc = 2,
            def = -1,
        } },
    );
}

test "decode struct from array w/ duplicate field" {
    try expectDecode(
        "\xa3\x00\x19\xfe\xed\x00\x19\xde\xad\x01\x19\xbe\xef",
        struct { abc: usize, def: usize },
        error.DuplicateField,
        .{},
    );
}

test "decode struct from array w/ duplicate field keeping first" {
    try expectDecode(
        "\xa3\x00\x19\xfe\xed\x00\x19\xde\xad\x01\x19\xbe\xef",
        struct { abc: usize, def: usize },
        .{ .abc = 0xfeed, .def = 0xbeef },
        .{ .duplicates = .First },
    );
}

test "decode struct from array w/ duplicate field keeping last" {
    try expectDecode(
        "\xa3\x00\x19\xfe\xed\x00\x19\xde\xad\x01\x19\xbe\xef",
        struct { abc: *const usize, def: usize },
        .{ .abc = &@as(usize, 0xdead), .def = 0xbeef },
        .{ .duplicates = .Last },
    );
}

test "decode struct from array w/ error" {
    try expectDecode(
        "\x82\x01\x20",
        struct { abc: *const usize, def: *const usize },
        error.UnexpectedToken,
        .{},
    );
}

test "decode struct from map w/ error" {
    try expectDecode(
        "\xa2\x00\x01\x01\x20",
        struct { abc: *const usize, def: *const usize },
        error.UnexpectedToken,
        .{},
    );
}

test "decode array w/ error" {
    try expectDecode("\x01", struct {}, error.UnexpectedToken, .{});
}

test "decode optional" {
    try expectDecode("\xf6", ?usize, null, .{});
    try expectDecode("\xf6", ?????usize, null, .{});
    try expectDecode("\x01", ?usize, 1, .{});
    try expectDecode("\x01", ?????usize, 1, .{});
}

test "decode enum" {
    try expectDecode("\x61\x61", enum { a, b }, .a, .{});
    try expectDecode("\x61\x62", enum { a, b }, .b, .{});
    try expectDecode("\x00", enum { a, b }, .a, .{});
    try expectDecode("\x01", enum { a, b }, .b, .{});
    try expectDecode("\x20", enum(isize) { a = -1, b = 1 }, .a, .{});
    try expectDecode("\x01", enum(isize) { a = -1, b = 1 }, .b, .{});
    try expectDecode("\x01", enum { a, b }, error.UnexpectedToken, .{ .integer = false });
    try expectDecode("\x61a", enum { a, b }, error.UnexpectedToken, .{ .text = false });
    try expectDecode("\x61c", enum { a, b }, error.UnknownTag, .{});
    try expectDecode("\x02", enum { a, b }, error.UnknownTag, .{});
    try expectDecode("\x02", enum(isize) { a = -1, b = 1 }, error.UnknownTag, .{});
    try expectDecode("\x63foo", enum { a, b }, error.UnknownTag, .{});
    try expectDecode("\x61f", enum { a, b }, error.UnknownTag, .{});
}

test "decode tagged union from external encoding" {
    try expectDecode(
        "\xa1\x00\x20",
        union(enum) { signed: isize, unsigned: usize },
        .{ .signed = -1 },
        .{ .encoding = .{ .External = .{} } },
    );
}

test "decode tagged union from array encoding" {
    try expectDecode(
        "\x82\x00\x20",
        union(enum) { signed: isize, unsigned: usize },
        .{ .signed = -1 },
        .{},
    );
}

test "decode tagged union from tagged encoding" {
    try expectDecode(
        "\xc0\x20",
        union(enum) { signed: isize, unsigned: usize },
        .{ .signed = -1 },
        .{ .encoding = .Tagged },
    );
    try expectDecode(
        "\xc2\x20",
        union(enum) { signed: isize, unsigned: usize },
        error.UnknownTag,
        .{ .encoding = .Tagged },
    );
}

test "decode tagged union with no items" {
    try expectDecode(
        "\xa0",
        union(enum) { signed: isize, unsigned: usize },
        error.ExpectedTag,
        .{ .encoding = .{ .External = .{} } },
    );
    try expectDecode(
        "\x80",
        union(enum) { signed: isize, unsigned: usize },
        error.ExpectedTag,
        .{},
    );
}

test "decode tagged union with extraneous items" {
    try expectDecode(
        "\xa2\x00\x20\x01\x01",
        union(enum) { signed: *const isize, unsigned: usize },
        error.TooManyItems,
        .{ .encoding = .{ .External = .{} } },
    );
    try expectDecode(
        "\x83\x00\x20\x01",
        union(enum) { signed: *const isize, unsigned: usize },
        error.TooManyItems,
        .{},
    );
}

fn expectSkip(bytes: []const u8) !void {
    var stream = std.io.fixedBufferStream(bytes);
    const reader = stream.reader();
    var data_item = try nextDataItem(reader);
    try data_item.skip();
    try std.testing.expectEqual(bytes.len, stream.pos);
}

test "skip" {
    try expectSkip("\x00");
    try expectSkip("\x43foo");
    try expectSkip("\x5f\x42fo\x41o\xff");
    try expectSkip("\x63foo");
    try expectSkip("\x7f\x62fo\x61o\xff");
    try expectSkip("\x83\x81\x00\x43foo\x20");
    try expectSkip("\x9f\x81\x00\x43foo\x20\xff");
    try expectSkip("\xa2\x61\x61\x00\x20\x81\x00");
    try expectSkip("\xbf\x61\x61\x00\x20\x81\x00\xff");
    try expectSkip("\xc1\x00");
    try std.testing.expectError(error.Malformed, expectSkip("\xbf\x00\xff"));
    try std.testing.expectError(error.Malformed, expectSkip("\xff"));
}

fn expectDiagnostic(bytes: []const u8, exp: anyerror![]const u8) !void {
    var stream = std.io.fixedBufferStream(bytes);
    const reader = stream.reader();
    const data_item = try nextDataItem(reader);
    var buf = std.ArrayList(u8).init(std.testing.allocator);
    defer buf.deinit();
    const res = data_item.diagnostic(std.testing.allocator, buf.writer());
    if (exp) |exp_str| {
        try std.testing.expectEqualStrings(exp_str, buf.items);
    } else |exp_err| {
        try std.testing.expectError(exp_err, res);
    }
}

test "diagnostic" {
    try expectDiagnostic("\x01", "1");
    try expectDiagnostic("\x20", "-1");
    try expectDiagnostic("\x00", "0");
    try expectDiagnostic("\x43foo", "h\"666f6f\"");
    try expectDiagnostic("\x5f\x42fo\x41o\xff", "(_ h\"666f\", h\"6f\")");
    try expectDiagnostic("\x63foo", "\"foo\"");
    try expectDiagnostic("\x62\"\\", "\"\\\"\\\\\"");
    try expectDiagnostic("\x62ü", "\"ü\"");
    try expectDiagnostic("\x63水", "\"水\"");
    try expectDiagnostic("\x7f\x62fo\x61o\xff", "(_ \"fo\", \"o\")");
    try expectDiagnostic("\x83\x81\x00\x43foo\x20", "[[0], h\"666f6f\", -1]");
    try expectDiagnostic("\x9f\x81\x00\x43foo\x20\xff", "[_ [0], h\"666f6f\", -1]");
    try expectDiagnostic("\xa2\x61\x61\x00\x20\x81\x00", "{\"a\": 0, -1: [0]}");
    try expectDiagnostic("\xbf\x61\x61\x00\x20\x81\x00\xff", "{_ \"a\": 0, -1: [0]}");
    try expectDiagnostic("\xc1\x00", "1(0)");
    try expectDiagnostic("\xf4", "false");
    try expectDiagnostic("\xf5", "true");
    try expectDiagnostic("\xf6", "null");
    try expectDiagnostic("\xe0", "simple(0)");
    try expectDiagnostic("\xf9\x3e\x00", "1.5e+00");
    try expectDiagnostic("\x9f\xff", "[_ ]");
}

/// A reader for CBOR byte strings.
///
/// It may be either of definite length or of indefinite length with embedded definite length
/// chunks.
pub fn ByteStringReader(comptime R: type) type {
    return ChunkReader(R, .ByteString);
}

/// A reader for CBOR text.
///
/// It may be either of definite length or of indefinite length with embedded definite length
/// chunks.
///
/// WARNING: There is no UTF-8 validation performed.
pub fn TextReader(comptime R: type) type {
    return ChunkReader(R, .Text);
}

fn ChunkReader(comptime R: type, comptime kind: DataItemTag) type {
    switch (kind) {
        .ByteString, .Text => {},
        else => @compileError(@tagName(kind) ++ " is not valid for ChunkReader"),
    }
    return union(enum) {
        const Self = @This();

        Definite: std.io.LimitedReader(R),
        Indefinite: struct {
            reader: R,
            chunk: ?std.io.LimitedReader(R) = null,
            fuse: bool = false,

            /// Read the next CBOR data item and return a reader for that chunk.
            ///
            /// WARNING: This is incompatible with `ChunkReader.read`.
            pub fn next(self: *@This()) Error(R)!?std.io.LimitedReader(R) {
                if (self.fuse) return null;
                const token = try nextToken(self.reader);
                if (std.meta.eql(token, .{ .Misc = .Break })) {
                    self.fuse = true;
                    return null;
                }
                switch (try token.intoDataItem(self.reader)) {
                    kind => |inner| switch (inner) {
                        .Indefinite => return error.Malformed,
                        .Definite => |chunk| return chunk,
                    },
                    else => return error.Malformed,
                }
            }
        },

        const Reader = std.io.Reader(*Self, Error(R), read);

        /// Return an `std.io.Reader` which reads the contents of the string.
        ///
        /// Chunk boundaries are handled implictly. The reader acts as if the stream ended when the
        /// end of the CBOR string is reached.
        pub inline fn reader(self: *Self) Reader {
            return .{ .context = self };
        }

        /// Read from the string. Chunk boundaries are handled implicitly for the caller.
        pub fn read(self: *Self, dest: []u8) Error(R)!usize {
            if (self.* == .Definite) return try self.Definite.read(dest);
            const indef = &self.Indefinite;
            var idx: usize = 0;
            while (idx < dest.len) {
                if (indef.chunk == null) {
                    indef.chunk = try indef.next() orelse return idx;
                }
                const nread = try indef.chunk.?.read(dest[idx..]);
                if (nread == 0) indef.chunk = null;
                idx += nread;
            }
            return idx;
        }

        /// Return the size of the string, if known.
        ///
        /// If the string is the `Indefinite` variant, then the size is unknown.
        pub inline fn sizeHint(self: Self) ?usize {
            return switch (self) {
                // SAFETY: bytes_left should be bounded by the usize which was used to create the
                // limited reader.
                .Definite => |fixed| @as(usize, @intCast(fixed.bytes_left)),
                else => null,
            };
        }

        /// Read until the end of this string.
        pub fn exhaust(self: *Self) !void {
            // TODO: Is there a good len for this? 512 is consistent with the buf_size of
            // `skipBytes`.
            var buf: [512]u8 = undefined;
            while (try self.read(&buf) > 0) {}
        }
    };
}

fn expectRead(
    bytes: []const u8,
    comptime kind: DataItemTag,
    expected: anyerror![]const u8,
) !void {
    var stream = std.io.fixedBufferStream(bytes);
    var str = try expectDataItem(stream.reader(), kind);
    if (expected) |exp| {
        const reader = str.reader();
        for (exp) |e| {
            try std.testing.expectEqual(e, try reader.readByte());
        }
    } else |e| {
        try std.testing.expectError(e, str.exhaust());
    }
}

test "ChunkReader" {
    try expectRead("\x43foo", .ByteString, "foo");
    try expectRead("\x5f\x42fo\x41o\xff", .ByteString, "foo");
    try expectRead("\x5f\x5f\x43foo\xff\xff", .ByteString, error.Malformed);
    try expectRead("\x5f\x00\xff", .ByteString, error.Malformed);
    try expectRead("\x63foo", .Text, "foo");
    try expectRead("\x7f\x62fo\x61o\xff", .Text, "foo");
    try expectRead("\x7f\x7f\x63foo\xff\xff", .Text, error.Malformed);
    try expectRead("\x7f\x00\xff", .Text, error.Malformed);
}

/// An iterator over data items.
pub fn Sequence(comptime R: type) type {
    return struct {
        const Self = @This();

        reader: R,
        remaining: ?usize,

        /// Read a data item and decode it into `T`.
        ///
        /// WARNING: Unexpected data items are skipped in their entirety. This has the potential
        /// of blocking for indefinite or otherwise large data items which act as containers. The
        /// tradeoff being that the stream is left at the next data item. If this is undesired
        /// behavior, then handle the variants manually.
        pub inline fn decode(
            self: *Self,
            comptime T: type,
            alloc: ?std.mem.Allocator,
            comptime opts: DecodeOptions(T),
        ) (DecodeError(R, T) || Error(R))!?T {
            var item = try self.next() orelse return null;
            return try item.decode(T, alloc, opts);
        }

        /// Read the next data item from the stream if there is any, unwrapping it as `tag`.
        ///
        /// Skips the data item and errors if the data item read from the stream is unexpected.
        ///
        /// WARNING: Unexpected data items are skipped in their entirety. This has the potential
        /// of blocking for indefinite or otherwise large data items which act as containers. The
        /// tradeoff being that the stream is left at the next data item. If this is undesired
        /// behavior, then handle the variants manually.
        pub inline fn expect(
            self: *Self,
            comptime tag: DataItemTag,
        ) (Error(R) || error{UnexpectedToken})!?std.meta.TagPayload(DataItem(R), tag) {
            var item = try self.next() orelse
                return null;
            switch (item) {
                tag => |payload| return payload,
                else => |*payload| {
                    try payload.skip();
                    return error.UnexpectedToken;
                },
            }
        }

        /// Read the next data item from the stream if there is any.
        pub fn next(self: *Self) Error(R)!?DataItem(R) {
            if (self.remaining) |rem| {
                if (rem == 0) {
                    return null;
                }
                self.remaining = rem - 1;
                return try nextDataItem(self.reader);
            }
            const token = try nextToken(self.reader);
            if (std.meta.eql(token, .{ .Misc = .Break })) {
                return null;
            }
            return try token.intoDataItem(self.reader);
        }

        pub inline fn sizeHint(self: Self) ?usize {
            return self.remaining;
        }

        /// Read all items remaining and skip them.
        ///
        /// `next` or `decode` must not have previously produced `null`.
        pub fn exhaust(self: *Self) !bool {
            var item = try self.next() orelse
                return false;
            while (true) {
                try item.skip();
                item = try self.next() orelse
                    break;
            }
            return true;
        }
    };
}

test "Sequence" {
    var definite_stream = std.io.fixedBufferStream("\x83\x20\x00\x20");
    var definite_seq = try expectDataItem(definite_stream.reader(), .Array);
    try std.testing.expectEqual(@as(?isize, -1), try definite_seq.decode(isize, null, .{}));
    try std.testing.expectEqual(@as(?usize, 0), try definite_seq.expect(.Unsigned));
    try std.testing.expectError(error.UnexpectedToken, definite_seq.expect(.Unsigned));
    try std.testing.expectEqual(try definite_seq.next(), null);
    var indefinite_stream = std.io.fixedBufferStream("\x9f\x20\x00\x20\xff");
    var indefinite_seq = try expectDataItem(indefinite_stream.reader(), .Array);
    try std.testing.expectEqual(@as(?isize, -1), try indefinite_seq.decode(isize, null, .{}));
    try std.testing.expectEqual(@as(?usize, 0), try indefinite_seq.expect(.Unsigned));
    try std.testing.expectError(error.UnexpectedToken, indefinite_seq.expect(.Unsigned));
    try std.testing.expectEqual(try indefinite_seq.next(), null);
}

/// A description of the contained data item.
pub fn Tagged(comptime R: type) type {
    return struct {
        const Self = @This();

        tag: usize,
        reader: R,

        /// Read the tagged contents from the stream, unwrapping it as `tag`.
        ///
        /// Skips the data item and errors if the data item read from the stream is unexpected.
        ///
        /// WARNING: Unexpected data items are skipped in their entirety. This has the potential
        /// of blocking for indefinite or otherwise large data items which act as containers. The
        /// tradeoff being that the stream is left at the next data item. If this is undesired
        /// behavior, then handle the variants manually.
        pub inline fn expect(
            self: *Self,
            comptime tag: DataItemTag,
        ) (Error(R) || error{UnexpectedToken})!std.meta.TagPayload(DataItem(R), tag) {
            var item = try self.content();
            switch (item) {
                tag => |payload| return payload,
                else => |*payload| {
                    try payload.skip();
                    return error.UnexpectedToken;
                },
            }
        }

        /// Return the contained data item.
        pub inline fn content(self: Self) !DataItem(R) {
            return try nextDataItem(self.reader);
        }
    };
}

test "Tagged" {
    var stream = std.io.fixedBufferStream("\xc0\x20");
    var tagged = try expectDataItem(stream.reader(), .Tag);
    try std.testing.expectEqual(@as(usize, 0), try tagged.expect(.Negative));
}

/// Read a data item and decode it into `T`.
///
/// WARNING: Unexpected data items are skipped in their entirety. This has the potential of
/// blocking for indefinite or otherwise large data items which act as containers. The tradeoff
/// being that the stream is left at the next data item. If this is undesired behavior, then
/// handle the variants manually.
pub inline fn decode(
    reader: anytype,
    comptime T: type,
    alloc: ?std.mem.Allocator,
    comptime opts: DecodeOptions(T),
) (DecodeError(@TypeOf(reader), T) || Error(@TypeOf(reader)))!T {
    var item = try nextDataItem(reader);
    return try item.decode(T, alloc, opts);
}

/// Read the next data item from the stream, unwrapping it as `tag`.
///
/// Skips the data item and errors if the data item read from the stream is unexpected.
///
/// WARNING: Unexpected data items are skipped in their entirety. This has the potential of
/// blocking for indefinite or otherwise large data items which act as containers. The tradeoff
/// being that the stream is left at the next data item. If this is undesired behavior, then
/// handle the variants manually.
pub inline fn expect(
    reader: anytype,
    comptime tag: DataItemTag,
) (Error(@TypeOf(reader)) || error{UnexpectedToken})!std.meta.TagPayload(
    DataItem(@TypeOf(reader)),
    tag,
) {
    var item = try next(reader);
    switch (item) {
        tag => |payload| return payload,
        else => |*payload| {
            try payload.skip();
            return error.UnexpectedToken;
        },
    }
}
/// Rename expect for internal use.
const expectDataItem = expect;

test "expect" {
    var stream = std.io.fixedBufferStream("\x82\xa1\x00\x20\x61\x61");
    try std.testing.expectError(error.UnexpectedToken, expectDataItem(stream.reader(), .Negative));
}

/// Decode the next data item from the stream.
///
/// The returned data items may contain a copy of `reader`, so ensure the stream to which `reader`
/// points lives longer than the data item.
pub fn next(reader: anytype) Error(@TypeOf(reader))!DataItem(@TypeOf(reader)) {
    const token = try nextToken(reader);
    return try token.intoDataItem(reader);
}
/// Rename next for internal use.
const nextDataItem = next;

/// Read the next CBOR token from the stream.
fn nextToken(reader: anytype) !Token {
    const header = try reader.readStruct(Header);
    switch (header.major) {
        .Misc => return .{
            .Misc = switch (header.arg) {
                0x00...0x17 => .{ .Simple = header.arg },
                0x18 => .{ .Simple = try reader.readInt(u8, .big) },
                inline 0x19...0x1b => |arg| blk: {
                    const bits = comptime 8 << (arg - 0x18);
                    const Float = @Type(.{ .Float = .{ .bits = bits } });
                    const Int = @Type(.{ .Int = .{ .bits = bits, .signedness = .unsigned } });
                    break :blk .{
                        .Float = @as(Float, @bitCast(try reader.readInt(Int, .big))),
                    };
                },
                0x1c...0x1e => return error.Malformed,
                0x1f => .Break,
            },
        },
        inline else => |tag| {
            const Payload = std.meta.TagPayload(Token, tag);
            const optional = comptime @typeInfo(Payload) == .Optional;
            const arg: Payload = switch (header.arg) {
                0x00...0x17 => header.arg,
                inline 0x18...0x1b => |arg| blk: {
                    const bits = comptime 8 << (arg - 0x18);
                    const Int = @Type(.{ .Int = .{ .bits = bits, .signedness = .unsigned } });
                    break :blk std.math.cast(usize, try reader.readInt(Int, .big)) orelse
                        return error.Overflow;
                },
                0x1c...0x1e => return error.Malformed,
                0x1f => if (optional)
                    null
                else
                    return error.Malformed,
            };
            return @unionInit(Token, @tagName(tag), arg);
        },
    }
}

fn expectToken(bytes: []const u8, token: anyerror!Token) !void {
    var stream = std.io.fixedBufferStream(bytes);
    try std.testing.expectEqual(token, nextToken(stream.reader()));
}

test "nextToken" {
    // This should cover all tokens with definite arguments.
    try expectToken("\x00", .{ .Unsigned = 0 });
    try expectToken("\x17", .{ .Unsigned = 0x17 });
    try expectToken("\x18\x18", .{ .Unsigned = 0x18 });
    try expectToken("\x19\x01\x00", .{ .Unsigned = 0x100 });
    try expectToken("\x1a\x00\x01\x00\x00", .{ .Unsigned = 0x10000 });
    if (@bitSizeOf(usize) >= 64) {
        try expectToken("\x1b\x00\x00\x00\x01\x00\x00\x00\x00", .{ .Unsigned = 0x100000000 });
    }
    inline for (.{ u8, u16, u32, u64 }, 0..) |Int, idx| {
        if (@bitSizeOf(usize) < @bitSizeOf(Int)) continue;
        const max: Int = std.math.maxInt(Int);
        try expectToken(&[_]u8{0x18 + idx} ++ @as([@sizeOf(Int)]u8, @bitCast(max)), .{ .Unsigned = max });
    }
    // Still need to cover the indefinite case.
    try expectToken("\x5f", .{ .ByteString = null });
    // Just sanity check the major decoding.
    try expectToken("\x21", .{ .Negative = 1 });
    try expectToken("\x41", .{ .ByteString = 1 });
    try expectToken("\x61", .{ .Text = 1 });
    try expectToken("\x81", .{ .Array = 1 });
    try expectToken("\xa1", .{ .Map = 1 });
    try expectToken("\xc1", .{ .Tag = 1 });
    // Test the misc tokens.
    try expectToken("\xe1", .{ .Misc = .{ .Simple = 1 } });
    try expectToken("\xf8\xff", .{ .Misc = .{ .Simple = 255 } });
    try expectToken("\xf9\x3e\x00", .{ .Misc = .{ .Float = 1.5 } });
    try expectToken("\xfa\x49\x74\x24\x08", .{ .Misc = .{ .Float = 1000000.5 } });
    try expectToken("\xfb\x7e\x37\xe4\x3c\x88\x00\x75\x9c", .{ .Misc = .{ .Float = 1.0e+300 } });
    try expectToken("\xff", .{ .Misc = .Break });
    // Weird error cases.
    try expectToken("\x1c", error.Malformed);
    try expectToken("\xfc", error.Malformed);
    try expectToken("\x1f", error.Malformed);
}

/// Options for how to encode `T`.
pub fn EncodeOptions(comptime T: type) type {
    switch (@typeInfo(T)) {
        .Pointer => |info| switch (info.size) {
            .One => return EncodeOptions(info.child),
            .Many, .Slice => return struct {
                encoding: if (info.child == u8)
                    BytesEncoding
                else
                    ArrayEncoding = if (info.child == u8)
                    .ByteString
                else
                    .Array,
                child: EncodeOptions(info.child) = .{},
            },
            else => {},
        },
        inline .Array, .Vector => |info| return EncodeOptions([]const info.child),
        .Struct => |info| return struct {
            encoding: enum {
                Array,
                MapInteger,
            } = if (info.is_tuple)
                .Array
            else
                .MapInteger,
            fields: FieldOpts(info.fields, EncodeOptions) = .{},
            labels: ?type = null,
        },
        .Optional => |info| return EncodeOptions(info.child),
        .Enum => return struct {
            encoding: EnumEncoding = .Integer,
        },
        .Union => |info| return struct {
            encoding: union(enum) {
                External: EnumEncoding,
                Array: EnumEncoding,
                Tagged,
            } = .{ .Array = .Text },
            fields: FieldOpts(info.fields, EncodeOptions) = .{},
        },
        else => {},
    }
    return struct {};
}

const BytesEncoding = enum {
    Text,
    ByteString,
    Array,
};

const ArrayEncoding = enum { Array };

const EnumEncoding = enum { Text, Integer };

/// Encode `T` into `writer`.
///
/// `opts` disambiguates how to encode `value` when there are multiple possible representations.
pub fn encode(value: anytype, writer: anytype, comptime opts: EncodeOptions(@TypeOf(value))) !void {
    const T = @TypeOf(value);
    switch (@typeInfo(T)) {
        .Void => {
            try encodeHeader(.Misc, 0x16, writer);
            return;
        },
        .Bool => {
            try encodeHeader(.Misc, 0x14 + @as(u8, @intFromBool(value)), writer);
            return;
        },
        .ComptimeInt, .Int => {
            if (value < 0) {
                try encodeHeader(.Negative, @as(usize, @intCast(-(value + 1))), writer);
            } else {
                try encodeHeader(.Unsigned, @as(usize, @intCast(value)), writer);
            }
            return;
        },
        .ComptimeFloat, .Float => {
            inline for (.{ f16, f32, f64 }) |Float| {
                const converted = @as(Float, @floatCast(value));
                if ((comptime @TypeOf(value) == Float) or value == converted) {
                    const Int = @Type(.{ .Int = .{
                        .bits = @bitSizeOf(Float),
                        .signedness = .unsigned,
                    } });
                    const int = @as(Int, @bitCast(converted));
                    const big = std.mem.nativeToBig(Int, int);
                    const bytes = std.mem.asBytes(&big);
                    const byte_order = std.math.log2(@as(u8, bytes.len));
                    try writer.writeByte(@as(u8, @intFromEnum(Major.Misc)) << 5 | 0x18 + byte_order);
                    try writer.writeAll(bytes);
                    return;
                }
            }
        },
        .Pointer => |info| switch (info.size) {
            .One => {
                try encode(value.*, writer, opts);
                return;
            },
            .Many => if (std.meta.sentinel(T)) |sentinel| {
                const len = std.mem.indexOfSentinel(info.child, sentinel, value);
                try encode(value[0..len], writer, .{
                    .encoding = opts.encoding,
                    .child = opts.child,
                });
                return;
            },
            .Slice => if (opts.encoding == .Array) {
                try encodeHeader(.Array, value.len, writer);
                for (value) |v|
                    try encode(v, writer, opts.child);
                return;
            } else if (opts.encoding == .Text) {
                std.debug.assert(std.unicode.utf8ValidateSlice(value));
                try encodeHeader(.Text, value.len, writer);
                try writer.writeAll(value);
                return;
            } else if (opts.encoding == .ByteString) {
                try encodeHeader(.ByteString, value.len, writer);
                try writer.writeAll(value);
                return;
            },
            else => {},
        },
        inline .Array, .Vector => |info| {
            const slice: []const info.child = &@as([info.len]info.child, value);
            return try encode(slice, writer, opts);
        },
        .Struct => |info| switch (opts.encoding) {
            .Array => {
                try encodeHeader(.Array, info.fields.len, writer);
                inline for (info.fields) |fld|
                    try encode(@field(value, fld.name), writer, @field(opts.fields, fld.name));
                return;
            },
            .MapInteger => {
                const FieldEnum = opts.labels orelse std.meta.FieldEnum(T);
                comptime {
                    for (std.meta.fieldNames(T)) |name| {
                        std.debug.assert(@hasField(FieldEnum, name));
                    }
                    for (std.meta.fieldNames(FieldEnum)) |name| {
                        std.debug.assert(@hasField(T, name));
                    }
                }
                try encodeHeader(.Map, info.fields.len, writer);
                inline for (comptime sortedLabels(FieldEnum)) |label| {
                    try encode(label, writer, .{ .encoding = .Integer });
                    try encode(
                        @field(value, @tagName(label)),
                        writer,
                        @field(opts.fields, @tagName(label)),
                    );
                }
                return;
            },
        },
        .Null => return try encodeHeader(.Misc, 0x16, writer),
        .Optional => {
            if (value) |inner| {
                try encode(inner, writer, opts);
            } else {
                try encode(null, writer, .{});
            }
            return;
        },
        .Enum => switch (opts.encoding) {
            .Text => {
                try encode(@tagName(value), writer, .{ .encoding = .Text });
                return;
            },
            .Integer => {
                try encode(@intFromEnum(value), writer, .{});
                return;
            },
        },
        .Union => switch (opts.encoding) {
            inline .External, .Array => |tag_encoding, encoding| {
                const container_type = if (encoding == .External)
                    .Map
                else
                    .Array;
                const container_len = @as(usize, 1) + @intFromBool(encoding == .Array);
                try encodeHeader(container_type, container_len, writer);
                try encode(@as(std.meta.Tag(T), value), writer, .{ .encoding = tag_encoding });
                switch (value) {
                    inline else => |payload, ct_tag| try encode(
                        payload,
                        writer,
                        @field(opts.fields, @tagName(ct_tag)),
                    ),
                }
                return;
            },
            .Tagged => {
                try encodeHeader(.Tag, @intFromEnum(value), writer);
                switch (value) {
                    inline else => |payload, ct_tag| try encode(
                        payload,
                        writer,
                        @field(opts.fields, @tagName(ct_tag)),
                    ),
                }
                return;
            },
        },
        else => {},
    }
    @compileError(@typeName(T) ++ " may not be generically encoded");
}

/// Test whether `value` successfully encodes into `hex`.
fn expectEncode(
    comptime T: type,
    value: T,
    comptime exp: []const u8,
    comptime opts: EncodeOptions(T),
) !void {
    var array = std.ArrayList(u8).init(std.testing.allocator);
    defer array.deinit();
    try encode(value, array.writer(), opts);
    errdefer std.debug.print("Literal: \"{any}\"\n", .{std.zig.fmtEscapes(array.items)});
    try std.testing.expectEqualSlices(u8, exp, array.items);
}

test "encode void" {
    try expectEncode(void, {}, "\xf6", .{});
}

test "encode bool" {
    try expectEncode(bool, false, "\xf4", .{});
    try expectEncode(bool, true, "\xf5", .{});
}

test "canonical encoding int" {
    try expectEncode(usize, 0x00, "\x00", .{});
    try expectEncode(usize, 0x17, "\x17", .{});
    try expectEncode(usize, 0x18, "\x18\x18", .{});
    try expectEncode(usize, 0xff, "\x18\xff", .{});
    try expectEncode(usize, 0x0100, "\x19\x01\x00", .{});
    try expectEncode(usize, 0xffff, "\x19\xff\xff", .{});
    try expectEncode(usize, 0x0001_0000, "\x1a\x00\x01\x00\x00", .{});
    try expectEncode(usize, 0xffff_ffff, "\x1a\xff\xff\xff\xff", .{});
    if (@bitSizeOf(usize) >= 64) {
        try expectEncode(usize, 0x0000_0001_0000_0000, "\x1b\x00\x00\x00\x01\x00\x00\x00\x00", .{});
        try expectEncode(usize, 0xffff_ffff_ffff_ffff, "\x1b\xff\xff\xff\xff\xff\xff\xff\xff", .{});
    }
    try expectEncode(comptime_int, 0x00, "\x00", .{});
    try expectEncode(comptime_int, 0x17, "\x17", .{});
    try expectEncode(comptime_int, 0x18, "\x18\x18", .{});
    try expectEncode(comptime_int, 0xff, "\x18\xff", .{});
    try expectEncode(comptime_int, 0x0100, "\x19\x01\x00", .{});
    try expectEncode(comptime_int, 0xffff, "\x19\xff\xff", .{});
    try expectEncode(comptime_int, 0x0001_0000, "\x1a\x00\x01\x00\x00", .{});
    try expectEncode(comptime_int, 0xffff_ffff, "\x1a\xff\xff\xff\xff", .{});
    if (@bitSizeOf(comptime_int) >= 64) {
        try expectEncode(comptime_int, 0x0000_0001_0000_0000, "\x1b\x00\x00\x00\x01\x00\x00\x00\x00", .{});
        try expectEncode(comptime_int, 0xffff_ffff_ffff_ffff, "\x1b\xff\xff\xff\xff\xff\xff\xff\xff", .{});
    }
}

test "canonical encoding float" {
    try expectEncode(f64, 1.5, "\xf9\x3e\x00", .{});
    try expectEncode(f64, 1000000.5, "\xfa\x49\x74\x24\x08", .{});
}

test "encode pointer" {
    try expectEncode(*const usize, &0xdeadbeef, "\x1a\xde\xad\xbe\xef", .{});
}

test "encode slice as byte string" {
    try expectEncode(
        []const u8,
        "Hello, world!",
        "MHello, world!",
        .{},
    );
}

test "encode slice as text" {
    try expectEncode(
        []const u8,
        "Hello, world!",
        "mHello, world!",
        .{ .encoding = .Text },
    );
}

test "encode many pointer" {
    try expectEncode(
        [*:0]const u8,
        "Hello, world!",
        "mHello, world!",
        .{ .encoding = .Text },
    );
}

test "encode slice as array" {
    try expectEncode(
        []const u8,
        "Hello, world!",
        "\x8d\x18H\x18e\x18l\x18l\x18o\x18,\x18 \x18w\x18o\x18r\x18l\x18d\x18!",
        .{ .encoding = .Array },
    );
    try expectEncode(
        []const usize,
        &[_]usize{ 0xdeadbeef, 0xbeefdead },
        "\x82\x1a\xde\xad\xbe\xef\x1a\xbe\xef\xde\xad",
        .{},
    );
}

test "encode array" {
    try expectEncode([3]u8, "abc".*, "Cabc", .{});
}

test "encode vector" {
    try expectEncode(@Vector(3, u8), "abc".*, "Cabc", .{});
}

test "encode struct as array" {
    try expectEncode(struct {}, .{}, "\x80", .{ .encoding = .Array });
    try expectEncode(
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        "\x82\x19\xde\xad\x19\xbe\xef",
        .{ .encoding = .Array },
    );
}

test "encode struct as map" {
    try expectEncode(struct {}, .{}, "\xa0", .{ .encoding = .MapInteger });
    try expectEncode(
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        "\xa2\x00\x19\xde\xad\x01\x19\xbe\xef",
        .{ .encoding = .MapInteger },
    );
    try expectEncode(
        struct { abc: usize, def: usize },
        .{ .abc = 0xdead, .def = 0xbeef },
        "\xa2\x02\x19\xde\xad\x20\x19\xbe\xef",
        .{ .encoding = .MapInteger, .labels = enum(isize) {
            abc = 2,
            def = -1,
        } },
    );
}

test "encode null" {
    try expectEncode(@TypeOf(null), null, "\xf6", .{});
}

test "encode optional" {
    try expectEncode(?isize, -1, "\x20", .{});
    try expectEncode(?isize, null, "\xf6", .{});
}

test "encode enum" {
    try expectEncode(enum { a, b }, .a, "\x00", .{});
    try expectEncode(enum { a, b }, .b, "\x01", .{});
    try expectEncode(enum(u8) { a, b = 0xff }, .b, "\x18\xff", .{});
    try expectEncode(enum { a, b }, .a, "aa", .{ .encoding = .Text });
}

test "encode enum as external encoding" {
    try expectEncode(
        union(enum) { abc: isize, def: usize },
        .{ .abc = -1 },
        "\xa1\x63abc\x20",
        .{ .encoding = .{ .External = .Text } },
    );
    try expectEncode(
        union(enum) { abc: isize, def: usize },
        .{ .abc = -1 },
        "\xa1\x00\x20",
        .{ .encoding = .{ .External = .Integer } },
    );
}

test "encode enum as array encoding" {
    try expectEncode(
        union(enum) { abc: isize, def: usize },
        .{ .abc = -1 },
        "\x82\x63abc\x20",
        .{},
    );
    try expectEncode(
        union(enum) { abc: isize, def: usize },
        .{ .abc = -1 },
        "\x82\x00\x20",
        .{ .encoding = .{ .Array = .Integer } },
    );
}

test "encode enum as tagged encoding" {
    try expectEncode(
        union(enum) { abc: isize, def: usize },
        .{ .abc = -1 },
        "\xc0\x20",
        .{ .encoding = .Tagged },
    );
}

/// Encode `int` in the "preferred serialization" as defined in RFC8949 Section 4.2.1. Essentially,
/// `int` is encoded in the smallest format possible without loss.
pub fn encodeHeader(major: Major, maybe_int: ?usize, writer: anytype) !void {
    const major_int: u8 = @as(u8, @intFromEnum(major)) << 5;
    const int = maybe_int orelse {
        try writer.writeByte(major_int | 0x1f);
        return;
    };
    if (int < 0x18) {
        try writer.writeByte(major_int | @as(u8, @intCast(int)));
        return;
    }
    comptime var bits = 8;
    inline while (bits <= 64) : (bits *= 2) {
        const Int = @Type(.{ .Int = .{
            .bits = bits,
            .signedness = .unsigned,
        } });
        if (std.math.cast(Int, int)) |i| {
            const order = comptime std.math.log2(bits / 8);
            try writer.writeByte(major_int | 0x18 + order);
            try writer.writeInt(Int, i, .big);
            return;
        }
    }
}

test "encodeHeader" {
    var buf: [64]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buf);
    const writer = stream.writer();

    try encodeHeader(.Array, null, writer);
    try encode(1, writer, .{});
    try encodeBreak(writer);
    try std.testing.expectEqualSlices(u8, "\x9f\x01\xff", stream.getWritten());
    stream.reset();
}

/// Write a break token to the given `writer`.
///
/// This is only used to terminate indefinite `.ByteString`, `.Text`, `.Map`, or `.Array`.
pub fn encodeBreak(writer: anytype) !void {
    try writer.writeByte(0xff);
}

/// Comptime generate a slice of all `L` sorted using deterministic encoding rules.
///
/// Deterministic ordering of labels as defined in RFC 8949 uses the "bytewise lexographic order of
/// their deterministic encodings". In this case, `L` is an enum, and its integer value is encoded.
///
/// This is intended as a utility to help when implementing an encoding function for a map which has
/// comptime known labels.
///
/// Ref: https://www.rfc-editor.org/rfc/rfc8949.html#section-4.2.1-2.3.1
pub fn sortedLabels(comptime L: type) []const L {
    comptime {
        const values = std.enums.values(L);
        var res: [values.len]L = undefined;
        @memcpy(&res, values);
        std.mem.sort(L, &res, {}, canonicalLessThan(L, L));
        return &res;
    }
}

test "sortedLabels" {
    const E = enum(isize) {
        a = -1,
        b = 10,
        c = 100,
    };
    try std.testing.expectEqualSlices(
        E,
        &[_]E{ .b, .c, .a },
        comptime sortedLabels(E),
    );
}

fn canonicalLessThan(comptime I1: type, comptime I2: type) fn (void, I1, I2) bool {
    return struct {
        fn canonicalLessThanImpl(_: void, a: I1, b: I2) bool {
            var a_enc = std.BoundedArray(u8, 9){};
            encode(a, a_enc.writer(), .{}) catch unreachable;
            var b_enc = std.BoundedArray(u8, 9){};
            encode(b, b_enc.writer(), .{}) catch unreachable;
            return bytewiseLexographicLessThan(a_enc.slice(), b_enc.slice());
        }
    }.canonicalLessThanImpl;
}

fn bytewiseLexographicLessThan(a: []const u8, b: []const u8) bool {
    return for (a, 0..) |a_byte, idx| {
        const b_byte = b[idx];
        if (a_byte != b_byte) break a_byte < b_byte;
    }
    // SAFETY: Quoting from RFC 8949:
    // > the self-delimiting nature of the CBOR encoding means that there are no two well-formed
    // > CBOR encoded data items where one is a prefix of the other. The bytewise lexicographic
    // > comparison of deterministic encodings of different map keys therefore always ends in a
    // > position where the byte differs between the keys, before the end of a key is reached.
    else unreachable;
}

test "bytewiseLexographicLessThan" {
    const order = .{
        "\x0a",
        "\x18\x64",
        "\x20",
        "\x61\x7a",
        "\x62\x61\x61",
        "\x81\x18\x64",
        "\x81\x20",
        "\xf4",
    };
    comptime var idx = 0;
    inline while (idx < order.len - 1) : (idx += 1) {
        const a = order[idx];
        const b = order[idx + 1];
        try std.testing.expect(bytewiseLexographicLessThan(a, b));
    }
}

/// Escape double quotes, newlines, carriage returns, and tabs. Everything else is passed through.
fn semiJsonUtf8Escape(reader: anytype, writer: anytype) !void {
    while (true) {
        var codepoint_buf: [4]u8 = undefined;
        codepoint_buf[0] = reader.readByte() catch |err| {
            if (err == error.EndOfStream) break;
            return err;
        };
        const ulen = try std.unicode.utf8ByteSequenceLength(codepoint_buf[0]);
        if (ulen > 1) {
            try reader.readNoEof(codepoint_buf[1..ulen]);
        }
        switch (try std.unicode.utf8Decode(codepoint_buf[0..ulen])) {
            '"' => try writer.writeAll("\\\""),
            '\n' => try writer.writeAll("\\n"),
            '\r' => try writer.writeAll("\\r"),
            '\t' => try writer.writeAll("\\t"),
            '\\' => try writer.writeAll("\\\\"),
            else => try writer.writeAll(codepoint_buf[0..ulen]),
        }
    }
}

fn hexStream(inner: anytype) HexStream(@TypeOf(inner)) {
    return HexStream(@TypeOf(inner)){ .inner = inner };
}

fn HexStream(comptime W: type) type {
    return struct {
        const Self = @This();

        pub const Error = W.Error;

        pub const Writer = std.io.Writer(*Self, W.Error, write);

        inner: W,

        pub fn write(self: *Self, bytes: []const u8) Self.Error!usize {
            for (bytes) |byte| {
                try self.inner.print("{x:0>2}", .{byte});
            }
            return bytes.len;
        }

        pub fn writer(self: *Self) Writer {
            return Writer{ .context = self };
        }
    };
}

fn genericFree(alloc: ?std.mem.Allocator, value: anytype) void {
    const T = @TypeOf(value);
    switch (@typeInfo(T)) {
        .Pointer => |info| switch (info.size) {
            .One => alloc.?.destroy(value),
            .Slice => alloc.?.free(value),
            else => @compileError("unable to generically free " ++ @typeName(T)),
        },
        .Optional => if (value) |v| genericFree(alloc, v),
        .Struct => |info| inline for (info.fields) |fld| {
            genericFree(alloc, @field(value, fld.name));
        },
        .Union => switch (value) {
            inline else => |payload| genericFree(alloc, payload),
        },
        else => {},
    }
}

fn FieldOpts(comptime source_fields: anytype, comptime recurse: anytype) type {
    var fields: [source_fields.len]std.builtin.Type.StructField = undefined;
    inline for (&fields, source_fields) |*opt_fld, val_fld| {
        const Field = recurse(val_fld.type);
        opt_fld.* = .{
            .name = val_fld.name,
            .type = Field,
            .default_value = @ptrCast(&Field{}),
            .is_comptime = false,
            .alignment = @alignOf(Field),
        };
    }
    return @Type(.{ .Struct = .{
        .layout = .Auto,
        .fields = &fields,
        .decls = &.{},
        .is_tuple = false,
    } });
}
